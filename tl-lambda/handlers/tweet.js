'use strict';

module.exports.api = async (event) => {
  const Twitter = require('twitter');
  const body = JSON.parse(event.body);
  const client = new Twitter({
    consumer_key: process.env.CONSUMER_KEY,
    consumer_secret: process.env.CONSUMER_SECRET,
    access_token_key: body['token'],
    access_token_secret: body['secret']
  });

  return new Promise((resolve, reject) => {
    client.post('statuses/update', {'status': body['text']})
          .then(tweet => {
            resolve({
              statusCode: 200,
              headers: {
                "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS
              },
              body: JSON.stringify({
                message: tweet,
                input: event,
              }),
            });
          })
        .catch(err => {
          console.log(err);
          reject({
            statusCode: 500,
            headers: {
              "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
              "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS
            },
            body: JSON.stringify({
              message: err,
              input: event,
            }),
          });
        });
  });
};
