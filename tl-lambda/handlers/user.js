'use strict';

module.exports.api = async (event) => {
  const params = event.pathParameters;
  const Twitter = require('twitter');

  const client = new Twitter({
    consumer_key: process.env.CONSUMER_KEY,
    consumer_secret: process.env.CONSUMER_SECRET,
    access_token_key: params['token'],
    access_token_secret: params['secret']
  });

  return new Promise((resolve, reject) => {
     client.get('users/show', { user_id : params['uid'] }).
                 then((user) => {
                   resolve({
                     statusCode: 200,
                     headers: {
                       "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                       "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS
                     },
                     body: JSON.stringify({
                       message: {user},
                       input: event
                     }),
                   })
                 })
                .catch(err => {
                  console.log(err);
                  reject( {
                    statusCode: 404,
                    headers: {
                      "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                      "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS
                    },
                    body: JSON.stringify({
                      message: 'User Not Found',
                      input: event,
                    }),
                  })
                });
  });


};
