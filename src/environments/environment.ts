// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
  API_ENDPOINT_BASE: 'https://9dljdxvcvi.execute-api.us-east-1.amazonaws.com/dev',
  firebase: {
    apiKey: "AIzaSyBv_b1mCrB76syMtb3eo9g-xzLStxRXmwA",
    authDomain: "twitter-lite-e3f7b.firebaseapp.com",
    databaseURL: "https://twitter-lite-e3f7b.firebaseio.com",
    projectId: "twitter-lite-e3f7b",
    storageBucket: "twitter-lite-e3f7b.appspot.com",
    messagingSenderId: "762735266488"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
