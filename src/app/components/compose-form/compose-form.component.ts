import {Component, ElementRef, EventEmitter, OnInit, Output, Renderer2} from '@angular/core';
import {UserService} from '../../services/user/user.service';
import {TwitterService} from '../../services/twitter/twitter.service';

@Component({
  selector: 'twitter-lite-compose-form',
  templateUrl: './compose-form.component.html',
  styleUrls: ['./compose-form.component.scss']
})
export class ComposeFormComponent implements OnInit {
  tweet: string;
  textareaRows = 1;
  @Output() formSubmit: EventEmitter<string> = new EventEmitter<string>();

  constructor(private _el: ElementRef, private _renderer: Renderer2) { }

  ngOnInit() {

    document.addEventListener('click', (e: any) => {

      if (!e.target.classList.contains('tweet-btn') && !e.target.classList.contains('tweet-field') && !this.tweet) {
          this.offFieldFocus();
      }
    });
  }

  onFieldFocus() {
    const card = document.querySelector('.user-card');
    const actionsEl = this._el.nativeElement.querySelector('.actions');

    this.textareaRows = 4;
    this._renderer.setStyle(actionsEl, 'display', 'block');
    this._renderer.addClass(card, 'form-focus');
  }

  offFieldFocus() {
    const card = document.querySelector('.user-card');

    this.textareaRows = 1;
    const actionsEl = this._el.nativeElement.querySelector('.actions');
    this._renderer.setStyle(actionsEl, 'display', 'none');
    this._renderer.removeClass(card, 'form-focus');
  }

}
