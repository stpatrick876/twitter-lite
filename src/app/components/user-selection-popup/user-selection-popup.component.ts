import {Component, ElementRef, OnInit, Renderer2} from '@angular/core';
import {UserService} from '../../services/user/user.service';
import {User} from '../../models/User';

@Component({
  selector: 'twitter-lite-user-selection-popup',
  templateUrl: './user-selection-popup.component.html',
  styleUrls: ['./user-selection-popup.component.scss']
})
export class UserSelectionPopupComponent {
  user: User;

  constructor(private _el: ElementRef, private _renderer: Renderer2, private _userService: UserService) { }

  showPopup() {
    const modal = this._el.nativeElement.querySelector('#userSelectionPopup');

    if(modal) {
      this._renderer.setStyle(modal, 'display', 'block');
    }

   this.user = this._userService.activeUser;
  }

  hidePopup() {
    const modal = this._el.nativeElement.querySelector('#userSelectionPopup');
    this._renderer.setStyle(modal, 'display', 'none');
  }

  launchTwitterAuth() {
    this._userService.doTwitterLogin().then(res => {
      this._userService.setActiveUser(res);
      this.hidePopup();
    });
  }
}
