import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { UserSelectionPopupComponent } from './user-selection-popup.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {UserService} from '../../services/user/user.service';
import {AngularFireAuth, AngularFireAuthModule} from 'angularfire2/auth';

describe('UserSelectionPopupComponent', () => {
  let component: UserSelectionPopupComponent;
  let fixture: ComponentFixture<UserSelectionPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSelectionPopupComponent ],
      imports: [HttpClientTestingModule, AngularFireAuthModule],
      providers: [UserService, {provide: AngularFireAuth, useValue: {}}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSelectionPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
