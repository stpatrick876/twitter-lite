import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {User} from '../../models/User';
import {TwitterService} from '../../services/twitter/twitter.service';
import * as moment from 'moment';
import {ComposeFormComponent} from '../compose-form/compose-form.component';

@Component({
  selector: 'twitter-lite-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit {
    @Input() user: User;
    @Output() tweetPosted: EventEmitter<boolean> = new EventEmitter<boolean>();
    @ViewChild(ComposeFormComponent) form: ComposeFormComponent;
  constructor(private _tweetSrv: TwitterService) { }

  ngOnInit() {
  }

  /**
   * Handle compose form submit
   */
  onFormSubmit(text: string) {
    const token = sessionStorage.getItem('tl_token');
    const secret = sessionStorage.getItem('tl_secret');

      const payload = {
        token,
        secret,
        text,
        displayName: this.user.screen_name
      };

      this._tweetSrv.postTweet(payload).subscribe(() => {
        this.form.tweet = '';
        this.form.offFieldFocus();
        this.tweetPosted.emit(true);
      });
  }

}
