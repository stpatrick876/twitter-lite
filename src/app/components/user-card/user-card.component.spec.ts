import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCardComponent } from './user-card.component';
import {ComposeFormComponent} from '../compose-form/compose-form.component';
import {FormsModule} from '@angular/forms';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {By} from '@angular/platform-browser';
import {TEST} from '../../contants';
import {TwitterService} from '../../services/twitter/twitter.service';
import {of} from 'rxjs';

describe('UserCardComponent', () => {
  let component: UserCardComponent;
  let fixture: ComponentFixture<UserCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserCardComponent, ComposeFormComponent ],
      imports: [HttpClientTestingModule, FormsModule],
      providers: [TwitterService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should make call to service for posting tweet on form submitted event',  () => {
    const onFormSubmitSpy = spyOn(component, 'onFormSubmit').and.callThrough();
    const postTweetSpy = spyOn(TestBed.get(TwitterService), 'postTweet').and.returnValue(of({}));
    const tweet = 'A test tweet';
    component.user = TEST.MOCK_USERS[0];
    const formDebugElement = fixture.debugElement.query(By.directive(ComposeFormComponent));
    formDebugElement.componentInstance.formSubmit.emit(tweet);

    expect(onFormSubmitSpy).toHaveBeenCalledWith(tweet);
    expect(postTweetSpy).toHaveBeenCalled();
  });
});
