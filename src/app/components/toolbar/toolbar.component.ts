import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'twitter-lite-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
  @Output() launchUserSelection: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
  }

}
