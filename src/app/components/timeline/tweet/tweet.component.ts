import {Component, Input, OnInit} from '@angular/core';
import {Tweet} from '../../../models/Tweet';

@Component({
  selector: 'twitter-lite-tweet',
  templateUrl: './tweet.component.html',
  styleUrls: ['./tweet.component.scss']
})
export class TweetComponent implements OnInit {
  @Input() tweet: Tweet;
  constructor() { }

  ngOnInit() {
  }

}
