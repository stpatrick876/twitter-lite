import {Component, Input, OnInit} from '@angular/core';
import {Tweet} from '../../models/Tweet';

@Component({
  selector: 'twitter-lite-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {
   @Input() timeline: Tweet[];
  constructor() { }

  ngOnInit() {

  }

}
