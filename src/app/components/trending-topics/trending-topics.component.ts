import {Component, Input, OnInit} from '@angular/core';
import {TwitterService} from '../../services/twitter/twitter.service';
import {TrendingTopic, TrendList} from '../../models/TrendingTopic';

@Component({
  selector: 'twitter-lite-trending-topics',
  templateUrl: './trending-topics.component.html',
  styleUrls: ['./trending-topics.component.scss']
})
export class TrendingTopicsComponent {
  @Input() trendList: TrendList;
}
