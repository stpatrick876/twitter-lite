import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrendingTopicsComponent } from './trending-topics.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TwitterService} from '../../services/twitter/twitter.service';

describe('TrendingTopicsComponent', () => {
  let component: TrendingTopicsComponent;
  let fixture: ComponentFixture<TrendingTopicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrendingTopicsComponent ],
      providers: [TwitterService],
      imports: [HttpClientTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrendingTopicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display trending topics ',  () => {
    component.trendList = {
      promotedTrends: [{
        name: 'a',
        url: 'test',
        promoted_content: true,
        tweet_volume: 12,
      }],
      otherTrends:  [{
        name: 'trend a',
        url: 'test',
        promoted_content: true,
        tweet_volume: 12,
      },{
        name: 'trend  b',
        url: 'test',
        promoted_content: true,
        tweet_volume: 12,
      }],
    };
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelectorAll('li').length).toBe(3);
  });
});
