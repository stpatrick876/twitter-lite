import {getTestBed, TestBed} from '@angular/core/testing';

import { TwitterService } from './twitter.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {environment} from '../../../environments/environment';

describe('TwitterService', () => {
  let injector: TestBed;
  let service: TwitterService;
  let httpMock: HttpTestingController;


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TwitterService]
    });
    injector = getTestBed();
    service = injector.get(TwitterService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
   httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should handle get trending topics api ',  () => {
    const mockRes = {
      message: [{
        trends: [{
          id: 1,
          name: 'a',
          promoted_content: true,
          tweet_volume: 150
        }, {
          id: 2,
          name: 'lorem ipsum',
          promoted_content: false,
          tweet_volume: 100
        }, {
          id: 3,
          name: 'xyz',
          promoted_content: false,
          tweet_volume: 200
        }]
      }]
    };

    service.getTrendingTopics().subscribe(res => {
      expect(res.otherTrends.length).toBe(2);
      expect(res.promotedTrends.length).toBe(1);
    });

    const req = httpMock.expectOne(`${environment.API_ENDPOINT_BASE}/trends/null/null`);
    expect(req.request.method).toBe("GET");
    req.flush(mockRes);
  });

  it('should handle get timeline ',  () => {
    const mockRes = {
      message: [{
          id: 1,
          name: 'a',
          promoted_content: true,
          tweet_volume: 150
        }, {
          id: 2,
          name: 'lorem ipsum',
          promoted_content: false,
          tweet_volume: 100
        }, {
          id: 3,
          name: 'xyz',
          promoted_content: false,
          tweet_volume: 200
        }]
    };

    service.getTimeline('userHandle').subscribe(res => {
      expect(res.length).toBe(3);
    });

    const req = httpMock.expectOne(`${environment.API_ENDPOINT_BASE}/timeline/null/null/userHandle`);
    expect(req.request.method).toBe("GET");
    req.flush(mockRes);
  });
});
