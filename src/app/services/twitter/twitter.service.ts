import { Injectable } from '@angular/core';
import {interval, Observable, of} from 'rxjs';
import {TrendingTopic, TrendList} from '../../models/TrendingTopic';
import {delay, map, repeat, switchMap, tap} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {API_ENDPOINT_BASE} from '../../contants';
import {Tweet} from '../../models/Tweet';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TwitterService {

  constructor(private _http: HttpClient) { }

  getTrendingTopics(): Observable<TrendList> {
    const token = sessionStorage.getItem('tl_token');
    const secret = sessionStorage.getItem('tl_secret');
    return this._http.get(`${environment.API_ENDPOINT_BASE}/trends/${token}/${secret}`)
      .pipe(map((res: any) => {
        const { trends } = res.message[0];

        let promotedTrends = trends.filter(trend => trend.promoted_content);
        let otherTrends = trends.filter(trend => !trend.promoted_content);

        promotedTrends = this.takeTop(promotedTrends, 1);
        otherTrends = this.takeTop(otherTrends, 9);
        return {
          promotedTrends,
          otherTrends
        };
      }));
  }

  takeTop = (trends: TrendingTopic[], count: number)  => trends.sort((a, b) => (a.tweet_volume > b.tweet_volume) ? 1 : -1)
                                                              .slice(0, count);
  getTimeline(displayName: string): Observable<Tweet[]> {
    const token = sessionStorage.getItem('tl_token');
    const secret = sessionStorage.getItem('tl_secret');
      return this._http.get(`${environment.API_ENDPOINT_BASE}/timeline/${token}/${secret}/${displayName}`)
        .pipe(
          tap(res => console.log('got timeline response: ', res)),
          map((res: any) => {
          return res.message as Tweet[];
        }));
  }

  postTweet(payload: any) {

    return this._http.post(`${environment.API_ENDPOINT_BASE}/tweet`, payload);
  }

}
