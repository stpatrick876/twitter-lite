import {getTestBed, TestBed} from '@angular/core/testing';

import { UserService } from './user.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {AngularFireAuthModule, AngularFireAuth} from 'angularfire2/auth';
import {environment} from '../../../environments/environment';

describe('UserService', () => {
  let injector: TestBed;
  let service: UserService;
  let httpMock: HttpTestingController;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AngularFireAuthModule],
      providers: [{provide: AngularFireAuth, useValue: {}}]
    });

    injector = getTestBed();
    service = injector.get(UserService);
    httpMock = injector.get(HttpTestingController);

    });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should handle get user',() => {
    const mockRes = {
      message: {
        user: {
          id: '123',
          screen_name: 'userHandle',
        }
      }
    };

    service.getUser('userHandle', '1234567896', '987654321').subscribe(user => {
      expect(user.id).toBe('123');
    });

    const req = httpMock.expectOne(`${environment.API_ENDPOINT_BASE}/user/1234567896/987654321/userHandle`);
    expect(req.request.method).toBe('GET');
    req.flush(mockRes);
  });

  it('should set Active User',  () => {
    const authUser =  {
      credential: {
        accessToken: 'ssm2333nddh9ddd',
        secret: 's39hdd93bbsbs2',
      },
      user: {
        id: '123',
        displayName: 'userHandle',
      }
    };

    const mockRes = {
      message: {
        user: {
          id: '123',
          screen_name: 'userHandle',
        }
      }
    };
    spyOn(service, 'getUser').and.callThrough();
    service.setActiveUser(authUser);
    const req = httpMock.expectOne(`${environment.API_ENDPOINT_BASE}/user/1234567896/987654321/userHandle`);
    expect(req.request.method).toBe('GET');
    req.flush(mockRes);
  });
});
