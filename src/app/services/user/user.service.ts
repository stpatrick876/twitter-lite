import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../../models/User';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private _activeUser$: BehaviorSubject<User | null> =  new BehaviorSubject(null);
  activeUser$: Observable<User> = this._activeUser$.asObservable();

  constructor(private _http: HttpClient, public afAuth: AngularFireAuth) { }



  getUser(uid: string, token: string, secret: string): Observable<User> {
    return this._http.get(`${environment.API_ENDPOINT_BASE}/user/${token}/${secret}/${uid}`)
                      .pipe(map((res: any) => res.message.user as User));
  }

  get activeUser (): User {
    return this._activeUser$.getValue();
  }

  setActiveUser (authResponse?: any) {
        let  token, secret, user;
        if(authResponse) {
           token = authResponse.credential.accessToken;
           secret = authResponse.credential.secret;
           user = authResponse.user;
          sessionStorage.setItem('tl_token', token);
          sessionStorage.setItem('tl_secret', secret);
          sessionStorage.setItem('tl_user', JSON.stringify(user));
        } else {
          token = sessionStorage.getItem('tl_token');
          secret = sessionStorage.getItem('tl_secret');
          user = JSON.parse(sessionStorage.getItem('tl_user'));
        }
       this.getUser(user.providerData[0].uid, token, secret).subscribe(res => {
         this._activeUser$.next(res);
       });
  }


  doTwitterLogin(){
    return new Promise<any>((resolve, reject) => {
      const provider = new firebase.auth.TwitterAuthProvider();
      this.afAuth.auth
        .signInWithPopup(provider)
        .then((result: any) => {
          resolve(result);
        }, err => {
          console.log(err);
          reject(err);
        });
    });
  }


}
