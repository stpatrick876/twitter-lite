import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {UserSelectionPopupComponent} from './components/user-selection-popup/user-selection-popup.component';
import {UserService} from './services/user/user.service';
import {User} from './models/User';
import {Tweet} from './models/Tweet';
import {TwitterService} from './services/twitter/twitter.service';
import {TrendList} from './models/TrendingTopic';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @ViewChild(UserSelectionPopupComponent) userSelectionPopupComponent: UserSelectionPopupComponent;
  user: User;
  timeline: Tweet[];
  trendList: TrendList;

  constructor(private _userSrv: UserService, private _twitterSrv: TwitterService) {}

  ngOnInit(): void {
    const activeUser = sessionStorage.getItem('tl_user');
    this._userSrv.activeUser$.subscribe((user: User) => {
      if(user) {
        this.user = user;
        this.fetchTrends();
        this.updateTimeline(user.screen_name);
      }
    });

    if (!activeUser) {
      this.onLaunchUserSelection();
    } else {
      this._userSrv.setActiveUser();
    }
  }

  updateTimeline(displayName: string) {
    this._twitterSrv.getTimeline(displayName).subscribe((tweets: Tweet[]) => {
      this.timeline = tweets;
    });
  }

  fetchTrends() {
    this._twitterSrv.getTrendingTopics().subscribe((trendList: TrendList) => {
      this.trendList = trendList;
    });
  }

  onLaunchUserSelection() {
      this.userSelectionPopupComponent.showPopup();
  }

  onTweetPosted(e) {
    if(e) {
      this.updateTimeline(this.user.screen_name);
    }
  }

}
