export interface TrendingTopic {
  name: string;
  url: string;
  promoted_content: boolean;
  tweet_volume: number;
}

export interface TrendList {
  promotedTrends: TrendingTopic[];
  otherTrends: TrendingTopic[];
}
