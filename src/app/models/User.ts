export interface User {
  id: string;
  name: string;
  screen_name: string;
  location?: string;
  verified: boolean;
  followers_count: number;
  friends_count: number;
  listed_count: number;
  favourites_count?: number;
  statuses_count: number;
  created_at: number;
  profile_image_url: string;
}
