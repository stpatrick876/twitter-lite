import {User} from './User';

export interface Tweet {
  id: number;
  text: string;
  user: User;
  retweeted: boolean;
  favorited: boolean;
  favorite_count: number;
  created_at: string;
}


