export const API_ENDPOINT_BASE = 'https://5c826aa72d2ad30014be51b4.mockapi.io/tl/api/v1';


export const TEST = {
  MOCK_USERS: [{
        "id": "1",
        "name": "Keven Lemke PhD",
        "screen_name": "Joana.Friesen95",
        "location": "HI",
        "verified": false,
        "followers_count": 12,
        "friends_count": 80,
        "listed_count": 81,
        "statuses_count": 35,
        "created_at": 1552087573,
        "profile_image_url": "https://s3.amazonaws.com/uifaces/faces/twitter/kudretkeskin/128.jpg"
      }, {
        "id": "2",
        "name": "Alford Jacobs",
        "screen_name": "Courtney_Will76",
        "location": "LA",
        "verified": false,
        "followers_count": 42,
        "friends_count": 21,
        "listed_count": 35,
        "statuses_count": 2,
        "created_at": 1552087513,
        "profile_image_url": "https://s3.amazonaws.com/uifaces/faces/twitter/carlfairclough/128.jpg"
  }]
}
