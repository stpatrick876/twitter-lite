import {TestBed, async} from '@angular/core/testing';
import { AppComponent } from './app.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {ToolbarComponent} from './components/toolbar/toolbar.component';
import {UserCardComponent} from './components/user-card/user-card.component';
import {TrendingTopicsComponent} from './components/trending-topics/trending-topics.component';
import {CompanyCardComponent} from './components/company-card/company-card.component';
import {TimelineComponent} from './components/timeline/timeline.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {UserService} from './services/user/user.service';
import {UserSelectionPopupComponent} from './components/user-selection-popup/user-selection-popup.component';
import {TwitterService} from './services/twitter/twitter.service';
import {AngularFireAuthModule, AngularFireAuth} from 'angularfire2/auth';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        ToolbarComponent,
        UserCardComponent,
        TrendingTopicsComponent,
        CompanyCardComponent,
        TimelineComponent,
        UserSelectionPopupComponent
      ],
      imports: [HttpClientTestingModule, AngularFireAuthModule],
      providers: [UserService, TwitterService, {
        provide: AngularFireAuth,
        useValue: {

        }

      }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));


  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });


  it('should launch user selection popup if user not found in session storage',  () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    spyOn(sessionStorage, 'getItem').and.returnValue(null);
    const popupSpy = spyOn(app, 'onLaunchUserSelection');
    app.ngOnInit();
    expect(popupSpy).toHaveBeenCalled();
  });

});
