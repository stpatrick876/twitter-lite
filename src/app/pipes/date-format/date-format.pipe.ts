import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

moment.locale('en', {
  relativeTime: {
    future: 'in %s',
    past: '%s',
    s:  'now',
    ss: '%ss',
    m:  'a minute',
    mm: '%dm',
    h:  'an hour',
    hh: '%dh',
    d:  'a day',
    dd: '%dd',
    M:  'a month',
    MM: '%dM',
    y:  'a year',
    yy: '%dY'
  }
});

@Pipe({
  name: 'dateFormat'
})
export class DateFormatPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const dt = new Date(value);
    return moment(dt).fromNow();
  }

}
