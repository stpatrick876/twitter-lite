import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AppComponent } from './app.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { UserCardComponent } from './components/user-card/user-card.component';
import { TrendingTopicsComponent } from './components/trending-topics/trending-topics.component';
import { CompanyCardComponent } from './components/company-card/company-card.component';
import { TimelineComponent } from './components/timeline/timeline.component';
import { UserSelectionPopupComponent } from './components/user-selection-popup/user-selection-popup.component';
import {TwitterService} from './services/twitter/twitter.service';
import {HttpClientModule} from '@angular/common/http';
import {UserService} from './services/user/user.service';
import { TweetComponent } from './components/timeline/tweet/tweet.component';
import { ComposeFormComponent } from './components/compose-form/compose-form.component';
import {FormsModule} from '@angular/forms';
import { DateFormatPipe } from './pipes/date-format/date-format.pipe';
import {environment} from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    UserCardComponent,
    TrendingTopicsComponent,
    CompanyCardComponent,
    TimelineComponent,
    UserSelectionPopupComponent,
    TweetComponent,
    ComposeFormComponent,
    DateFormatPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
  ],
  providers: [TwitterService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
