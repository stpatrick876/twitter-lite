# TwitterLite

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.3.

## Architecture

- Client
  - Angular 7
  - SCSS
- Server Side
  - lambda
  - nodejs
  - twitter api

## Urls
- gitlab - https://gitlab.com/stpatrick876/twitter-lite

- live - https://twiterlite.netlify.com/
